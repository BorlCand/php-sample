<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessagesController extends Controller {

    public function create(Request $request) {
        $this->validate($request, [
            'text' => 'required|string',
            'dialog_id' => 'required|integer|exists:dialogs,id',
            'interlocutor_id' => 'required|integer|exists:interlocutors,id',
            's_show_delay' => 'integer',
            's_order' => 'integer',
            's_align' => 'integer',
        ]);

        $message = new \App\Message();
        $message->text = $request->input('text');
        $message->dialog_id = $request->input('dialog_id');
        $message->interlocutor_id = $request->input('interlocutor_id');
        $message->s_show_delay = $request->input('s_show_delay');
        $message->s_order = $request->input('s_order');
        $message->s_align = $request->input('s_align');

        $message->save();

        return response()->success(compact('messages'));
    }

    public function index(Request $request) {
        $messages = \App\Dialog::find($request->input('id'))->messages;

        return response()->success(compact('messages'));
    }

    public function delete(Request $request) {
        $messages = \App\Message::find($request->input('id'));
        $messages->delete();

        return response()->success([]);
    }

}
