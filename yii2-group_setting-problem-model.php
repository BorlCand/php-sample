<?php

namespace app\models;

/*
 * Group Settings implementation
 * System has default group and individual groups
 * The table contains 5 000+ cells.
 * On the frontend each cell has value from the default group
 * Value can be overriden by setting from individual group
 * ==============
 * = Old solution
 * ==============
 * - create a multidimensional array (4 dim) and then iterate through it
 * = Problems:
 * - iterating through an array had been tooking many time (> 3 mins)
 * - it was hard to search bugs
 * - it was hard to understand source code
 * ==============
 * = New solution
 * ==============
 * on the frontend: 
 * - send only changed values
 * on the backend:
 * - create hash-array which contains all settings: 
 * both default and individual (to iterate on render)
 * - iterate through hash-array that contains only individual settings
 * with updated values
 */
class GroupItem extends BaseModel
{
    public static function tableName()
    {
        return 'group_item';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['created_at', 'edited_by', 'updated_at'], 'integer'],
            [['default'], 'boolean'],
            [['name'], 'string', 'max' => 64]
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
            'edited_by' => 'Кем',
            'default' => 'Базовая',
        ];
    }

    public function getCreated()
    {
	return date("d/m/Y",  strtotime($this->created_at));
    }

    public function getModified()
    {
	if($this->updated_at == '0000-00-00 00:00:00')
	    return "Не редактировалась";
	return date("d/m/Y",  strtotime($this->updated_at));
    }

    public function getGroupAssignments()
    {
        return $this->hasMany(GroupAssignment::className(), ['item_name' => 'name']);
    }

    public function getGroupSettings()
    {
        return $this->hasMany(GroupSetting::className(), ['group_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['edited_by' => 'id']);
    }


    private $individual = [];
    private $base = [];

    public function removeFromIndividual($key){
        unset($this->individual[$key]);
    }

    public static function generateHashString($params){
        $result = $params[0];
        unset($params[0]);

        foreach ($params as $value) {
            if ($value !== null){
                $result .= '.' . $value;
            }
        }

        return $result;
    }

    private function loadData()
    {
        // Individual group
        $data = $this->getGroupSettings()
          ->all();

        foreach($data as $item)
        {
          $this->individual[md5($this->generateHashString([$item['type'], $item['offer'], $item['country'], $item['platform']]))] = $item;
        }

        $showSettings = OffersEnabled::find()
          ->where(['group_id' => $this->id])
          ->all();

        if ($showSettings){
            foreach($showSettings as $item)
            {
              $this->individual[md5($this->generateHashString(['11', $item['offer']]))] = $item;
            }
        }

        // Base group
        $data = GroupSetting::find()
          ->where(['group_id' => 1])
          ->all();

        foreach($data as $item)
        {
          $this->base[md5($this->generateHashString([$item['type'], $item['offer'], $item['country'], $item['platform']]))] = $item;
        }

        $showSettings = OffersEnabled::find()
          ->where(['group_id' => 1])
          ->all();

        if ($showSettings){
            foreach($showSettings as $item)
            {
              $this->base[md5('11'.$item['offer'])] = $item;
            }
        }
    }

    public function getIndividual($type, $offer, $country = null, $platform = null)
    {
        return $this->getIndividualByHash(md5($this->generateHashString([$type,$offer,$country,$platform])));
    }

    public function getIndividualByHash($hash){
        if(empty($this->base)){
          $this->loadData();
        }

        return isset($this->individual[$hash]) ?
          $this->individual[$hash] : false;
    }

    public function getBase($type, $offer, $country = null, $platform = null)
    {
        return $this->getBaseByHash(md5($this->generateHashString([$type,$offer,$country,$platform])));
    }

    public function getBaseByHash($hash)
    {
        if(empty($this->base)){
          $this->loadData();
        }

        return isset($this->base[$hash]) ?
          $this->base[$hash] : false;
    }

    /* get Setting by Type, Offer, Country, Platform */
    public function getTOCPSetting($type, $offer, $country = null, $platform = null){
        return $this->getTOCPSettingByHash(md5($this->generateHashString([$type,$offer,$country,$platform])));
    }

    public function getTOCPSettingByHash($hash){
        if(empty($this->base)){
          $this->loadData();
        }
        
        $setting = $this->getIndividualByHash($hash);
        
        if($setting){
            return $setting;
        } else {
            return $this->getBaseByHash($hash);
        }
    }
}