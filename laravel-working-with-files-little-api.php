<?php

namespace App\Http\Controllers;

class WidgetController extends Controller {

    public function getGet() {
        return response(\File::get(public_path() . '/assets/js/init.js'), 200, ['Content-Type' => 'application/javascript; charset=UTF-8']);
    }

    public function getWidget() {
        return response(\File::get(public_path() . '/assets/js/widget.js'), 200, ['Content-Type' => 'application/javascript; charset=UTF-8']);
    }

    public function getStyle($type) {
        if ($type == \App\Type::TYPE_INLINE) {
            $file = 'style_inline.css';
        }

        if ($type == \App\Type::TYPE_MODAL) {
            $file = 'style_modal.css';
        }

        return response(\File::get(public_path() . '/assets/js/' . $file), 200, ['Content-Type' => 'text/css; charset=UTF-8']);
    }

    public function postSettings() {
        $request = [
            'site' => \Request::input('id'),
            'url' => \Request::input('url'),
            'date' => \Request::input('date'),
            'client_ip' => \Request::server('REMOTE_ADDR'),
            'referer' => app('Illuminate\Routing\UrlGenerator')->previous(),
        ];

        $site = \App\Site::find($request['site']);

        $remote_base_url = preg_replace('#^www\.(.+\.)#i', '$1', parse_url($request['url'], PHP_URL_HOST));

        if ($remote_base_url != $site->url) {
            return response('Bad request', 400);
        }

        $dialog = \App\Dialog::orderByRaw('RAND()')->with('messages.interlocutor.position', 'dialog_config')->first();

        $data = [
            'js' => action('WidgetController@getWidget'),
            'css' => action('WidgetController@getStyle', [$site->type->id]),
            'widgets' => [
                'TWidgetChat' => \App\Widget::createChatData($site, $dialog),
            ],
        ];

        return response()->json($data)->header('Access-Control-Allow-Origin', '*');
    }

}
