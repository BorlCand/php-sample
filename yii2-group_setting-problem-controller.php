<?php

namespace app\modules\v1\controllers;

use Yii;
use app\models\CurrencyRates;
use app\models\GroupAssignment;
use app\models\GroupItem;
use app\models\GroupSetting;
use app\models\Offers;
use app\models\OffersEnabled;
use app\models\User;
use yii\helpers\ArrayHelper;


/*
 * Problem:
 * * Table with about 5 000+ cells needs to be updated
 * * Table is the visual editor for group settings
 * * There are default group and individual groups
 */
class GroupsController extends BaseController {

    public function actionIndex() {
        $groups = GroupItem::getInstance()->all();

        return $this->render('list', [
            'groups' => $groups,
        ]);
    }

    public function actionCreate() {
        $model = new GroupItem();
        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = time();

            if ($model->save()) {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('add', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $offers = Offers::getInstance()->setFilters([['enable' => 1, 'parent_id' => 0]])->all();

        return $this->render(
                        'edit', [
                    'model' => $model,
                    'offers' => $offers,
                    'users' => ArrayHelper::map(
                            User::find()
                                    ->all(), 'id', 'email'
                    ),
                    'assignments' => array_keys(
                            ArrayHelper::map(
                                    GroupAssignment::find()
                                            ->andWhere(['group_id' => $id])
                                            ->all(), 'user_id', 'user_id'
                            )
                    ),
                    'countries' => CurrencyRates::getInstance()
                            ->all(),
                        ]
        );
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        $users = Yii::$app->request->post('users');

        if ($model->default != 1) {
            GroupAssignment::getInstance()->updateAll(['group_id' => 1], ['group_id' => $id]);

            $model->delete();
        }

        return $this->redirect(['index']);
    }

    public function actionSaveusers() {
        if (Yii::$app->request->post('group')) {
            $post_users = !empty(Yii::$app->request->post('users')) ? Yii::$app->request->post('users') : [];
            $group_id = Yii::$app->request->post('group');

            if (count($post_users) > 0) {
                $assignments = GroupAssignment::getInstance()->getQuery()->andWhere(['user_id' => $post_users]);

                if ($assignments->count() != count($post_users)) { // Не все пользователи из списка уже в этой группе, добавляем
                    $existing_assignments = $assignments->all() ? ArrayHelper::getColumn($assignments->all(), 'user_id') : [];

                    $new_assignments = array_diff($post_users, $existing_assignments);

                    foreach ($new_assignments as $user_id) {
                        $assignment = new GroupAssignment();
                        $assignment->group_id = 1;
                        $assignment->user_id = $user_id;
                        $assignment->created_at = time();
                        $assignment->save();
                    }
                }
            }
            GroupAssignment::getInstance()->updateAll(['group_id' => 1, 'created_at' => time()], ['group_id' => $group_id]);

            if ($post_users && !empty($post_users)) {
                GroupAssignment::getInstance()->updateAll(['group_id' => $group_id, 'created_at' => time()], ['user_id' => $post_users]);
            }
            return json_encode(['status' => 'success']);
        }
        return json_encode(['status' => 'error']);
    }

    public function actionSavegroup() {
        $group = Yii::$app->request->post('group');
        $data = Yii::$app->request->post('data') ? : [];

        if ($group) {
            $model = GroupItem::findOne($group);
            $model->updated_at = time() + 10800;
            $model->edited_by = Yii::$app->user->getIdentity()->id;
            $model->save();

            foreach ($data as $key => $value) {
                $set = $model->getIndividualByHash(md5($key));
                
                if ($set) {
                    if ($set instanceof OffersEnabled) {
                        if ($set->show != $value) {
                            $set->show = in_array($value, [0, 1]) ? $value : 1;
                            $set->save();
                        }
                    } else {
                        switch ($set->type) {
                            case GroupSetting::SETTING_TYPE_CPL_ENABLED:
                                if ($value == 0) {
                                    GroupSetting::deleteAll([
                                        'group_id' => $set->group_id,
                                        'offer' => $set->offer,
                                        'type' => GroupSetting::SETTING_TYPE_CPL_DATA
                                    ]);

                                    $model->removeFromIndividual(
                                            md5(
                                                    $model->generateHashString([GroupSetting::SETTING_TYPE_CPL_ENABLED, $set->offer])
                                            )
                                    );
                                    $set->delete();
                                }
                                break;
                            case GroupSetting::SETTING_TYPE_CPL_DATA:
                                if ($set->cpl != $value) {
                                    $set->cpl = empty($value) ? 0 : abs($value);
                                    $set->save();
                                }
                                break;
                        }
                    }
                } else {
                    $keyData = explode('.', $key);

                    switch ($keyData[0]) {
                        case GroupSetting::SETTING_TYPE_CPL_ENABLED:
                            if ($value == 1) {
                                $set = new GroupSetting();
                                $set->type = GroupSetting::SETTING_TYPE_CPL_ENABLED;
                                $set->group_id = $group;
                                $set->offer = $keyData[1];
                                $set->enabled = $value;
                                $set->created_at = time();
                                $set->save();
                            }
                            break;
                        case GroupSetting::SETTING_TYPE_CPL_DATA:
                            if (
                                    $model->getIndividual(GroupSetting::SETTING_TYPE_CPL_ENABLED, $keyData[1]) ||
                                    (
                                    isset($data[GroupSetting::SETTING_TYPE_CPL_ENABLED . '.' . $keyData[1]]) &&
                                    $data[GroupSetting::SETTING_TYPE_CPL_ENABLED . '.' . $keyData[1]] == 1
                                    )
                            ) {
                                $set = new GroupSetting();
                                $set->type = GroupSetting::SETTING_TYPE_CPL_DATA;
                                $set->group_id = $group;
                                $set->offer = $keyData[1];
                                $set->country = $keyData[2];
                                $set->platform = $keyData[3];
                                $set->cpl = empty($value) ? 0 : abs($value);
                                $set->created_at = time();
                                $set->save();
                            }
                            break;
                        case GroupSetting::SETTING_TYPE_SHOW:
                            $set = new OffersEnabled();
                            $set->group_id = $group;
                            $set->offer = $keyData[1];
                            $set->show = $value;
                            $set->save();
                            break;
                    }
                }
            }

            return json_encode(['status' => 'success']);
        }
        return json_encode(['status' => 'error']);
    }

    protected function findModel($id) {
        if (($model = GroupItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
