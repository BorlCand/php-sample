<?php

namespace app\utils;

use linslin\yii2\curl\Curl;

/*
 * Example of integration with CRM
 * 
 * * Example of using BaseModel singleton via derived class Status
 */
class CrmApi {

    private $apiUrl = 'URL';
    private $apiUsername = 'USERNAME';
    private $apiToken = 'TOKEN';
    
    private static $instance = null;
    
    private static $acceptedStatuses = ['paid', 'confirmed', 'tobeconfirmed', 'sent', 'complete'];
    private static $trackedStatuses = ['new', 'inwork', 'waiting', 'sent', 'tobeconfirmed'];
    private static $untrackedStatuses = ['paid', 'complete', 'cancel'];
    
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new CrmApi();
        }
        
        return self::$instance;
    }

    public function sendOrder($order) {
        return $this->getCurl()
            ->setOption(CURLOPT_POSTFIELDS, $order)
            ->setOption(CURLOPT_RETURNTRANSFER, 1)
            ->setOption(CURLOPT_CONNECTTIMEOUT, 180)
            ->setOption(CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($order)
            ])
            ->post($this->apiUrl);
    }

    public function getOrders($ids = []) {
        return $this->getCurl()
            ->setOption(CURLOPT_RETURNTRANSFER, 1)
            ->setOption(CURLOPT_COOKIESESSION, true)
            ->setOption(CURLOPT_CONNECTTIMEOUT, 180)
            ->setOption(CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ])
            ->get($this->apiUrl . '?user_email=' . $this->apiUsername . '&user_token=' . $this->apiToken . $this->prepareIds($ids));
    }

    public static function getStatuses() {
        return [
            /* Confirmed */
            'paid' => Status::getInstance()->PAID->id,
            'confirmed' => Status::getInstance()->CONFIRMED->id,
            'tobeconfirmed' => Status::getInstance()->SENT_TO_CONFIRMATION->id,
            /* In progress */
            'new' => Status::getInstance()->NEW->id,
            'waiting' => Status::getInstance()->WAITING->id,
            'inwork' => Status::getInstance()->IN_PROGRESS->id,
            'sent' => Status::getInstance()->SENT->id,
            /* Rejected */
            'duplicate' => Status::getInstance()->REJECTED->id,
            'incorrect' => Status::getInstance()->REJECTED->id,
            'cancel' => Status::getInstance()->REJECTED->id,
        ];
    }

    public static function getAcceptedStatuses() {
        return self::$acceptedStatuses;
    }

    public static function getTrackedStatuses() {
        return self::$trackedStatuses;
    }

    public static function getUntrackedStatuses() {
        return self::$untrackedStatuses;
    }

    /* CRM could not accept POST request */
    private function prepareIds($ids) {
        return '&order_ids[]=' . implode('&order_ids[]=', $ids);
    }

    private function getCurl() {
        return new Curl();
    }

}
