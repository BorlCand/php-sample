<?php

namespace app\models;

use \yii\db\ActiveRecord;

/*
 * Singleton was used here because of
 * 1. Easy global access to the instance
 * 2. Simplicity of adding memcached functionality in the future
 * 3. Existence of only one instance
 */

class BaseModel extends ActiveRecord {

    private $_filters = [];
    private $_objectsList = [];
    private $_objectsItems = [];
    
    private $fields = ['*'];
    private $where = [];
    
    protected $differentFilterable = ['limit', 'offset', 'created', 'datebegin', 'dateend', 'not_exists', 'or'];
    protected $uniqueId = 'id';
    protected $query = null;
    
    protected static $instance = [];

    public function getSelectFields() {
        $selectColumns = [];

        foreach ($this->getAttributes() as $column => $value) {
            if (!in_array($column, $this->excludeSelect)) {
                $selectColumns[] = $column;
            }
        }

        return $selectColumns;
    }

    public static function getInstance() {
        $classname = static::className();
        if (!isset(self::$instance[md5($classname)])) {
            $class = new $classname();
            $class->resetQuery($class->find());
            $class->initialize();
            self::$instance[md5($classname)] = $class;
        }
        return self::$instance[md5($classname)];
    }

    public static function isStaticCall() {
        $backtrace = debug_backtrace();
        return $backtrace[1]['type'] == '::';
    }

    public function initialize() {}

    public function getId() {
        return $this->primaryKey();
    }

    protected function saveRequestFilters($filters) {
        $this->_filters = $filters;
    }

    protected function getSavedFilters() {
        return $this->_filters;
    }

    public function getQuery() {
        $query = (new \yii\db\Query())->select(implode(',', $this->fields))->from(self::tableName());

        if (count($this->where) > 0) {
            $query->where($this->where);
        }

        return $query;
    }

    public function count() {
        return $this->getQuery()->count();
    }

    public function all() {
        if (!isset($this->_objectsList[get_class($this)])) {
            $this->_objectsList[get_class($this)] = [];
        }

        $this->_objectsList[get_class($this)] = $this->getQuery()->all();

        return empty($this->_objectsList[get_class($this)]) ? false : $this->_objectsList[get_class($this)];
    }

    public function one($toupper = false, $withoutCache = false) {
        if ($withoutCache == true) {
            return $this->getQuery()->one();
        }

        $id = isset($this->getQuery()->where[$this->uniqueId]) ? $this->getQuery()->where[$this->uniqueId] : '';

        if ($toupper) {
            $id = strtoupper($id);
        }

        $hash = md5(get_class($this));


        if (!isset($this->_objectsItems[$hash])) {
            $this->_objectsItems[$hash] = [];
        }


        if (!isset($this->_objectsItems[$hash][$id])) {
            $this->_objectsItems[$hash][$id] = $this->getQuery()->one();
        }

        return $this->_objectsItems[$hash][$id];
    }

    private function getWhere($condition, $params) {
        if (is_string($params[key($params)])) {
            if (count($parts = explode(',', $params[key($params)])) > 1) {
                $params[key($params)] = $parts;
            }
        }

        if ($condition == 'not') {
            return [$condition, [$this->getField($params) => $this->getValue($params)]];
        }

        if ($condition == '=') {
            return [$this->getField($params) => $this->getValue($params)];
        }

        return [$condition, $this->getField($params), $this->getValue($params)];
    }

    private function getField($params) {
        return key($params);
    }

    private function getValue($params) {
        return reset($params);
    }

    public function orderBy(array $orderBy = ['id' => SORT_ASC]) {
        $this->getQuery()->orderBy($orderBy);

        return $this;
    }

    public function groupBy($params = []) {
        $this->getQuery()->groupBy($params);

        return $this;
    }

    private function setFilter($condition, $params) {
        $field = $this->getField($params);
        $value = $this->getValue($params);

        if (!in_array($field, $this->differentFilterable)) {
            if (in_array($field, array_keys($this->getAttributes()))) {
                $this->getQuery()->andWhere($this->getWhere($condition, $params));
            } elseif (in_array($field, array_keys($this->renamedFields))) {
                $this->getQuery()->andWhere($this->getWhere($condition, [$this->renamedFields[$field] => $value]));
            }
        } else {
            if ($field == 'limit') {
                $this->getQuery()->limit($value);
            }

            if ($field == 'offset') {
                $this->getQuery()->offset($value);
            }

            if ($field == 'created') {
                $this->getQuery()->andWhere(['>=', 'created', date('Y-m-d 00:00:00', strtotime($value))]);
            }

            if ($field == 'datebegin') {
                $this->getQuery()->andWhere(['>=', 'created', date('Y-m-d 00:00:00', strtotime($value))]);
            }

            if ($field == 'dateend') {
                $this->getQuery()->andWhere(['<=', 'created', date('Y-m-d 23:59:59', strtotime($value))]);
            }

            if ($field == 'not_exists') {
                $this->getQuery()->andWhere(['not exists', $value]);
            }

            if ($field == 'or') {
                $this->getQuery()->andWhere(['or', $value]);
            }
        }
    }

    /*
     * Additionally added because it's the easy way to add multiple filters to the query
     */
    public function setFilters($conditions) {
        foreach ($conditions as $condition) {
            if (!is_array($condition)) {
                throw new \yii\base\Exception('Condition must be an array');
            }

            if (isset($condition[0]) && is_array($condition[1])) {
                $this->setFilter($condition[0], $condition[1]);
            }

            if (isset($condition[0]) && !is_array($condition[1]) && !is_object($condition[1])) {
                $this->setFilter($condition[0], [$condition[1] => $condition[2]]);
            }

            if (isset($condition[0]) && is_object($condition[1])) {
                $this->setFilter($condition[0], $condition[1]);
            }

            if (count($condition) > 1) {
                foreach ($condition as $key => $value) {
                    if (is_string($key)) {
                        $this->setFilter('=', [$key => $value]);
                    }
                }
            } else {
                $this->setFilter('=', [key($condition) => reset($condition)]);
            }
        }
        return $this;
    }

    public function resetFilters() {
        $this->query = $this->find();

        return $this;
    }

    public function union($objects) {
        $query = $this->getQuery();
        foreach ($objects as $object) {
            if ($object instanceof ActiveRecord) {
                $query->union($object->getQuery(), false);
            } else {
                throw new \yii\base\Exception('Incorrect object for union', 500);
            }
        }

        return $query;
    }

    public function setFields($fields) {
        $this->fields = $fields;
        $this->getQuery()->select($this->fields);

        return $this;
    }

    public function setWhere($where) {
        $this->where = $where;

        return $this;
    }

    public function resetQuery($query = null) {
        $this->setQuery($query);
    }

    public function getById($id) {
        $this->uniqueId = 'id';

        if (!isset($this->_objectsItems[get_class($this)])) {
            $this->_objectsItems[get_class($this)] = [];
        }

        if (!isset($this->_objectsItems[get_class($this)][$id])) {
            $this->_objectsItems[get_class($this)][$id] = $this->resetFilters()->setFilters([['id' => $id]])->one();
        }

        return $this->_objectsItems[get_class($this)][$id];
    }

    private function setQuery(\yii\db\ActiveQuery $query = null) {
        if (is_null($query)) {
            $this->query = $query;
        } else {
            $this->query = $this->find();
        }
    }

    private function __clone() {} // Deny clone

    private function __construct() {} // Deny new Model()

    private function __wakeup() {} // Deny unserialize

}
