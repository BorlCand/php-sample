<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InterlocutorsController extends Controller {

    public function create(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'position_id' => 'required|integer|exists:positions,id',
        ]);

        $interlocutor = new \App\Interlocutor();
        $interlocutor->name = $request->input('name');
        $interlocutor->user_id = \Auth::user()->id;
        $interlocutor->position_id = $request->input('position_id');

        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            $file = $request->file('avatar');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put($file->getFilename() . '.' . $extension, File::get($file));
            $interlocutor->avatar = $file->getFilename() . '.' . $extension;
        }

        $interlocutor->save();

        return response()->success(compact('interlocutor'));
    }

    public function get(Request $request) {
        $interlocutor = \App\Interlocutor::find($request->input('id'));

        return response()->success(compact('interlocutor'));
    }
    
    public function index() {
        $interlocutor = \App\User::find(\Auth::user()->id)->interlocutors;

        return response()->success(compact('interlocutor'));
    }

    public function delete(Request $request) {
        $interlocutor = \App\Interlocutor::find($request->input('id'));
        $interlocutor->delete();
        
        return response()->success([]);
    }

    public function edit(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'position_id' => 'required|integer|exists:positions,id',
        ]);

        $interlocutor = \App\Interlocutor::find($request->input('id'));
        $interlocutor->name = $request->input('name');
        $interlocutor->position_id = $request->input('position_id');
        $interlocutor->save();

        return response()->success(compact('interlocutor'));
    }

}
